# Makefile
SRC_DIR = forum
SHELL := /bin/bash
PROJECT = PythonAcademyForum
PY_VERSION = python3

tests:
	cd $(SRC_DIR) && $(PY_VERSION) -m coverage run --branch manage.py test --debug-mode --debug-sql \
	&& $(PY_VERSION) -m coverage report -m


dev-env-up:
	docker volume create --name=db-pg-data
	docker-compose -p $(PROJECT) -f docker/docker-compose.yaml up -d --build --remove-orphans
	@while [ $$(docker ps --filter health=starting --format "{{.Status}}" | wc -l) != 0 ]; do echo 'waiting for healthy containers'; sleep 1; done

dev-env-down:
	docker-compose -p $(PROJECT) -f docker/docker-compose.yaml down -v --remove-orphans

dev-env-clean: dev-env-down
	docker volume remove db-pg-data
